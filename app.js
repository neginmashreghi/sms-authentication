var express=require('express');
const path = require('path');
const bodyParser = require('body-parser');
require('dotenv').config()

var app=express();
app.use(bodyParser.urlencoded({ extended: false }));

// Twilio Credentials
var accountSid = process.env.TWILIO_ACCOUNT_SID;
var authToken = process.env.TWILIO_AUTH_TOKEN;
var current_pin ;

app.get('/', function(req, res) {
    res.sendFile(path.join(__dirname+'/smsForm.html'));
});

app.get('/pinForm',function(req,res){
    res.sendFile(path.join(__dirname+'/pinForm.html'));
}); 

 app.get('/send',function(req,res){
    // require the Twilio module and create a REST client
    current_pin = Math.floor(Math.random() * 100000);
    const phoneNumber = parseInt(req.query.phoneNum, 10)
    var client = require('twilio')(accountSid, authToken);
    client.messages.create(
    {
        to: '+1'+ phoneNumber ,
        from: process.env.TWILIO_NUMBER,
        body: current_pin+" is your pin code",
    },
    (err, message) => {
        if(err){
            console.log(err);
            res.end("error");
        }else{
            console.log("Message sent: " + message.body);
            res.end("sent");
        }
    }
    );

});

app.get('/verify',function(req,res){
    var pin = req.query.pin
    if( pin == current_pin){
        res.end("varified");
    }else {
        res.end("error");
    }
});



const port = 8003;
app.listen(port, () => console.log(`Server started on port ${port}`)); 

module.exports = app;