### Description
Node.js application that implement SMS authentication feature using Express, twilio.

---
### Installtion

To get the Node server running locally:

* Clone this repo
* ```npm install``` to install all required dependencies
* ```npm start``` to start the server 

---
### Dependencies
* [Express](https://expressjs.com/) - The server for handling and routing HTTP requests
* [twilio-node]( https://www.twilio.com/docs/libraries/node) - Helper library lets you write Node.js code to make HTTP requests to the Twilio API.
 
---

### Application Structure
* ```app.js``` - This file defines the Express server and routers to sends email and verifies pin verification
* ```smsForm.html```- This file contains an HTML form that takes an phone number and calls ```/send``` endpoint to send an SMS message with a unique pin.
* ```pinFrom.html```- This file contains HTML form that takes verification pin and calls ```/verify``` endpoint to check if the pin is valid or not.
